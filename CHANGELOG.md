# ✉️💣 Letterbomb - Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 4.1 - 2023/10/31
### Added
- The optional dependency `web`; this installs `letterbomb-web`.
- CLI `-u`, ``--updateoui`` flag.
### Changed
- Naturally raised exceptions now show the MAC instead.
### Removed
-`letterbomb/web` submodule; it has now been split into [`letterbomb-web`](https://pypi.org/project/letterbomb-web/). 

## 4.0 - 2023/10/21
### Added
- CI/CD now tests and will not continue if it fails.
- `.readthedocs.yaml` file.
- Version identifier to docs.
- `assets/pride.png` file.
- Images to docs.
- [Furo theme](https://github.com/pradyunsg/furo) to docs.
- `letterbomb.update_oui` will now report its progress.
- Type hints to `letterbomb.update_oui`.
- `tests/test_bomb`, an end-to-end test.
### Fixed
- `included/` not being in the source or build distribution.
- Typos of "BootMii" instead of "HackMii".
- Miscellaneous errors in docs.
- `letterbomb.update_oui` not backing-up correctly.
### Changed
- `letterbomb.REGION_LIST` is now `REGIONS`.
- `letterbomb.REGIONS` is now a `set[str]`.
- `letterbomb.InvalidMACError` exception is now `MACError`.
- `letterbomb.EmulatedMACError` exception is now `MACEmulatedError`.
- `letterbomb.MACEmulatedError` exception now subclasses `MACError`.
- `letterbomb.BadLengthMACError` exception is now `MACLengthError`.
- `letterbomb.MACLengthError` exception now subclasses `MACError`.
- `letterbomb.InvalidRegionError` exception is now `RegionError`.
- `letterbomb.web/` is now at `web/`.
- `static/` is now at `assets/`.
- `update_oui.py` is now at `letterbomb/update_oui.py`; it is included in the distribution and can be easily imported.
- Updated `tests/test_mac.py` and `tests/test_regions.py`.
- Updated README.md with badges and a new section on how to use the web-service.
### Removed
- Superfluous type hints.

## 3.6 - 2023/3/8
### Changed
* Changed changelog format
* Ported `update_oui.sh` to `update_oui.py`
* Fixed type annotations
* Fixed typo in documentation
* Made some small fixes in the web-service
* Changed license to MIT


### 3.5.2
<small>Released: *January 20th, 2023*</small><br/>
<small>Changes: *1*</small>
#### Web-service
* **Bumped Python requirement to 3.10**

### 3.5.1
<small>Released: *February 9th, 2022*</small><br/>
<small>Changes: *1*</small>
#### Module
* **Bumped minimum version to Python 3.9**

### 3.5.0
<small>Released: *February 9th, 2022*</small><br/>
<small>Changes: *7*</small>
#### Module
* **Fixed critical error where BootMii was not bundled correctly**
* **Merged `write_stream()` and `write_zip()` into one function, `write()`**
* **`write()` will now return the path of the resulting ZIP archive file if an output file is given**
#### Web-service
* **Minified some files**
#### CLI
* **Moved positional argument `outfile` to optional `-o`, `--outfile`**
* **Removed `-i` option entirely, replaced by the absense of `-o`**

### 3.4.0
<small>Released: *February 9th, 2022*</small><br/>
<small>Changes: *1*</small>
#### Module
* **Fixed bug where BootMii was not bundled correctly**

### 3.3.1
<small>Released: *February 8, 2022*</small><br/>
<small>Changes: *1*</small>
#### Module
* **Fixed error in `-i` CLI option**

### 3.3.0
<small>Released: *January 15th, 2021*</small><br/>
<small>Changes: *6*</small>
#### Web-service
* **Added option to show a forking ribbon**
* **Added option to show version**
* **Added option to show helpful links below image**
* **Added HTTP 405 error handler**
* **Simplified error message**
* **Doubled default Captcha timeout**

### 3.2.0
<small>Released: *November 25th, 2020*</small><br/>
<small>Changes: *1*</small>
* **Fixed `write_zip` and `write_stream` not writing paths correctly**

### 3.1.0
<small>Released: *November 24th, 2020*</small><br/>
<small>Changes: *9*</small>
##### Module
* **Added web documentation**
* **Removed WiiLoad**
##### Web-service
* **Fixed update triggering twice on key-handle event**
* **Fixed redundant `if` statements**
* **Key handling with inputs in the web-service now handle properly**
* **If JavaScript is disabled, the buttons will still be available**
* **Reworked JSON `/get` API**
* **Fixed alignment on mobile devices**
* **Fixed bad config parsing if BootMii option was unchecked**

### 3.0.6
<small>Released: *November 22nd, 2020*</small><br/>
<small>Changes: *2*</small>
* **Fixed `/get` when contacted with invalid parameters**
* **Fixed `/post` when contacted with invalid parameters**

### 3.0.5
<small>Released: *November 22nd, 2020*</small><br/>
<small>Changes: *1*</small>
* **Turned off debug mode for web-app**

### 3.0.4
<small>Released: *November 22nd, 2020*</small><br/>
<small>Changes: *1*</small>
* **Fixed BootMii toggle**

### 3.0.3
<small>Released: *November 22nd, 2020*</small><br/>
<small>Changes: *1*</small>
* **Added region error handling**


### 3.0.2
<small>Released: *November 22nd, 2020*</small><br/>
<small>Changes: *2*</small>
* **Fixed Flask deprecation warning for `warn()`**
* **Added type enforcement to `/get` and `/post` API**

### 3.0.1
<small>Released: *November 22nd, 2020*</small><br/>
<small>Changes: *3*</small>
* **Patched web-app JavaScript**
* **Patched README**
* **Fixed CI**

### 3.0.0
<small>Released: *November 22nd, 2020*</small><br/>
<small>Changes: *7*</small>
* **Re-added web-app, with improvements**
* **Fixed `write_stream`, it now properly writes to BytesIO**
* **Added exceptions**
* **Bomb packing will only occur after MAC is validated**
* **Added ability to write stream instead in CLI, `-i` flag**
* **Added Sphinx roles to documentation**
* **Detailed logs are on the web-app now**

### 2.0.1
<small>Released: *November 9th, 2020*</small><br/>
<small>Changes: *1*</small>
* **Fixed CI**

### 2.0.0
<small>Released: *November 9th, 2020*</small><br/>
<small>Changes: *7*</small>
* **Fixed README.md to be compatible with all readers**
* **Added documentation to source code**
* **Added additional documentation through Sphinx**
* **CI/CD now builds documentation and hosts it to [ReadTheDocs](https://rtfd.io)**
* **CI/CD now uploads PyPi packages to the GitLab package registry**
* **Split main method into functions**
* **You can now choose to write the zip archive through stdout, useful for [piping](https://en.wikipedia.org/wiki/Pipeline_(Unix))**

### 1.3.1
<small>Released: *November 7th, 2020*</small><br/>
<small>Changes: *1*</small>
* **Fixed MANIFEST.in**

### 1.3
<small>Released: *November 7th, 2020*</small><br/>
<small>Changes: *1*</small>
* **Fixed included files**

### 1.2.1
<small>Released: *November 7th, 2020*</small><br/>
<small>Changes: *1*</small>
* **Added badges to README**

### 1.2
<small>Released: *November 7th, 2020*</small><br/>
<small>Changes: *5*</small>
* **Fixed CI**
* **Fixed CLI**
* **Fixed README display**
* **Fixed project name**
* **Added `-v` to CLI**

### 1.1
<small>Released: *November 7th, 2020*</small><br/>
<small>Changes: *4*</small>
* **Added CI/CD**
* **Added CHANGELOG.md**
* **Added setup.py**
* **Added BootMii bundling**

### 1.0
<small>Released: *November 6th, 2020*</small><br/>
<small>Changes: *1*</small>
* **First release**
