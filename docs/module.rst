Using the module
================

LetterBomb is a normal Python module just like any other.
It can be imported and easily used as an API for other uses as well.

.. code:: python

	import letterbomb

	# Write to file, include HackMii:
	letterbomb.write(mac="mac address", region="region letter", output_file="letterbomb.zip")
	# Write to file, exclude HackMii:
	letterbomb.write(mac="mac address", region="region letter", pack_bundle=False, output_file="letterbomb.zip")

	# Write to BytesIO, include HackMii:
	letterbomb.write(mac="mac address", region="region letter")
	# Write to BytesIO, exclude HackMii:
	letterbomb.write(mac="mac address", region="region letter", pack_bundle=False)

	# To log debug messages
	letterbomb.LOGGING_LEVEL = letterbomb.logging.DEBUG
	# To log output to a file
	letterbomb.LOGGING_FILE = "log.txt"

For more detailed instructions and information, see :doc:`the reference documentation <autoapi/letterbomb/index>`.