Using the web-service
#####################

While LetterBomb does not **require** a network connection, you can run your own local version of `please.hackmii.com
<http://please.hackmii.com>`_.

The web-service is written in `Flask <https://pypi.org/project/Flask/>`_, but **it is not** included in the normal
``letterbomb`` module.
To install and run it:

.. code:: shell

	python -m pip install letterbomb_web
	python -m letterbomb_web

.. note::

	To bind to a port below 1000, run with superuser privileges.

.. warning::

	Don't use the above method for deployment servers,
	see `Flask docs <https://flask.palletsprojects.com/en/1.1.x/deploying/>`_.

Once bound, there are two ways of contacting the service:

POST
====

Go to http://localhost:<port> (`<http://localhost:5000>`_ by default).

Here is an image of it in action:

.. image:: images/webapp.webp
	:width: 500
	:align: center

1. Select a region, and enter in your MAC address letter by letter.
2. If you would like to include HackMii in the LetterBomb, check the option. **You should probably do this.**
3. If you configured a Captcha, complete it.
4. Cut any wire.

This POSTs the form to `/` using `Fetch <https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API>`_.
The webserver will return a response, either JSON data or the ZIP payload.

The URL's parameters will be changed to the choices made.
This does not add an entry to the history; it replaces the current one.
This can be used with :ref:`GET`.

For technical use, this is a summary of the POST request fields:

+---------------+----------------------------+---------------+----------------------+
| Field         | Description                | Expected Type | Example              |
+===============+============================+===============+======================+
| mac           | MAC address                | str           | :code:`eccd40dfb9a0` |
+---------------+----------------------------+---------------+----------------------+
| region        | Character of region        | str (char)    | :code:`U`, :code:`J` |
+---------------+----------------------------+---------------+----------------------+
| hackmii       | Bundle HackMii in archive? | any boolean   | :code:`1`, :code:`0` |
+---------------+----------------------------+---------------+----------------------+

Response
--------

If the request was successful, a payload will be sent. This is the LetterBomb ZIP file, compressed with Bzip2:9.

If the request failed, JSON will be sent.
It will have the format of:

.. code:: javascript

	{
		"success": false,
		"message": "Error message."
	}

GET
===

You can pass in URL parameters to pre-fill fields.
The parameter names are equal to the POST fields (``mac, region, hackmii``).

For example:

* :code:`?mac=000000000000&region=U&hackmii=1`
* :code:`?mac=000000000000&region=e&hackmii=no`
* :code:`?mac=000000000000&region=K&hackmii=yes`
* :code:`?mac=000000000000&region=j&hackmii=unchecked`

The `mac` param will always be truncated to 12 characters.
If `region` is not a valid region constant, none of the region boxes will be checked.

**The form still must be submitted manually.**