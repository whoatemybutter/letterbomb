MAC Addresses
#############

Each Wii has its own MAC address.
These addresses are core to identifying different Wiis, both to the other Wiis and to LetterBomb.

For LetterBomb to work, the Wii's MAC address must be embedded in the exploit file for the Wii to recognize it.

MAC addresses are 12 bytes in length, and look like this:

* :code:`AA:BB:CC:DD:EE:FF` or
* :code:`AABBCCDDEEFF`

LetterBomb will accept any valid Wii MAC address formats that match this regular expression:

:code:`^([0-9a-fA-F]{12})$`

**In other words, it must contain only A-F or 0-9, and be 12 characters in length.**

.. note::

	All valid Wii MAC addresses must also start with one of the sequences in an `OUI list
	<http://standards-oui.ieee.org/oui/oui.txt>`_ registered to Nintendo.

	Run the included `update_oui.sh
	<https://gitlab.com/whoatemybutter/letterbomb/-/blob/master/update_oui.sh>`_ to retrieve the newest copy of this list.

.. warning::
	If your MAC address is :code:`00:17:AB:99:99:99`, you are using an emulator such as `Dolphin
	<https://dolphin-emu.org/>`_. Use a real Wii.

For more information regarding the MAC address in other contexts, read:

* `Wikipedia <https://en.wikipedia.org/wiki/MAC_address>`_
* `IEEE - EUI <https://standards.ieee.org/content/dam/ieee-standards/standards/web/documents/tutorials/eui.pdf>`_
* `IEEE - MCGRP <https://standards.ieee.org/content/dam/ieee-standards/standards/web/documents/tutorials/macgrp.pdf>`_

.. _finding:

Finding your Wii's MAC
----------------------

1. On the Wii Channel menu, select the round Wii button on the bottom left of the screen.
2. Select :menuselection:`Wii Settings --> Next` and select :menuselection:`Internet --> Console Information`
	and then :guilabel:`View MAC Address`.

3. The first line will be the MAC address.

.. figure:: images/mac.png
	:width: 500px
	:align: center
