Using the CLI
#############

LetterBomb offers a CLI interface that can be used from the shell.
This is the easiest and most simple way to generate a LetterBomb.

To install and run it:

.. code-block:: sh

	python -m pip install -U letterbomb
	python -m letterbomb -h

This will print the help page for the CLI.
In summary, to generate a LetterBomb with HackMii bundled to ``letterbomb.zip``:

.. code:: shell

	python -m letterbomb MAC REGION -o letterbomb.zip -b

Extract the output ZIP archive to the SD card. Open the archive with any compatible program.
(`7zip <https://www.7-zip.org/>`_, `WinRAR <https://www.rarlab.com/download.htm>`_, `Ark <https://apps.kde
.org/en/ark>`_, etc.) Extract the entire archive contents directly to the SD card's root.

Verify extraction is completed. The card's contents should have the files & folders shown below.

.. figure:: images/archive.png

.. note:: The above images' specific dates, IDs, and timestamps do not need to match; only the general structure.

Install the LetterBomb to your Wii SD card.

1. Turn off your Wii first.
2. Remove the card from the computer and insert it into the Wii's SD slot until you hear a click.
3. Start your Wii normally.
4. Navigate towards the message board.
5. Scroll through the dates until you find a red mail icon with a bomb inside of it.
6. Select the letter.
7. Wait and install HomeBrew and/or BootMii, if you selected it.

.. figure:: images/sd_diagram.png