Requirements
############

LetterBomb has some requirements and limitations,
both for the system it is runnig on, and the Wii it will applied to.

System
======

.. figure:: images/sd.svg
	:width: 100
	:align: right

.. figure:: images/sdhc.svg
	:width: 100
	:align: right

* Python 3.10 or greater.
* An SD/SDHC **(not SDXC)** card formatted as either FAT16 or FAT32.

Wii
===

* Your Wii's MAC address (see :ref:`this <finding>`.)
* A Wii manufactured as RVL-001.
* System Menu 4.3 **(not 4.2 or lower)**.

.. important::
	LetterBomb cannot run on any System Menu below version **4.3**.
	To exploit a **4.2** or lower Wii, look into `Bannerbomb
	<https://wiibrew.org/wiki/Bannerbomb>`_ *(for 4.1)* or other related exploits.
