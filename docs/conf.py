#!/usr/bin/env python3
# coding=utf-8
import os
import sys

sys.path.insert(0, os.path.abspath(".."))

project = "LetterBomb"
author = "WhoAteMyButter"
project_copyright = f"2020, {author}, MIT"
master_doc = "index"
version = "4.0"

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.autodoc.typehints",
    "autoapi.extension",
    "sphinxext.opengraph",
    "sphinx.ext.autosectionlabel",
]

autoclass_content = "both"
autodoc_mock_imports = ["flask"]
autodoc_member_order = "groupwise"

autoapi_dirs = ["../letterbomb"]
autoapi_options = ["show-inheritance", "show-module-summary"]
autoapi_add_toctree_entry = False
autoapi_member_order = "groupwise"

ogp_use_first_image = True
ogp_enable_meta_description = True

templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

html_logo = "../assets/upscaled.png"
html_favicon = html_logo
html_theme = "furo"
html_theme_options = {"top_of_page_button": None}
html_css_files = [
    "custom.css",
]
