LetterBomb Documentation
########################

LetterBomb is an exploit for the Wii that is triggered using the Wii Message Board.

It is a safe way to enable homebrew on a Wii without hardware modification.
LetterBomb is achieved by use of the Wii Message Board which executes a homebrew application from an external SD card.
The exploit only works with any region of System Menu 4.3.

For more detailed information on the exploit itself, see `<https://wiibrew.org/wiki/LetterBomb>`_.

This module is a fork of the classic Wii hacking tool from fail0verflow.

Contents
--------

.. toctree::
	:titlesonly:
	:hidden:

	Home<self>

.. toctree::
	:caption: Installation

	requirements
	installing

.. toctree::
	:caption: Details

	mac_addresses
	how_it_works

.. toctree::
	:caption: Usage

	tutorial
	cli
	module
	web

.. toctree::
	:caption: Reference
	:maxdepth: 4
	:glob:

	autoapi/letterbomb/index