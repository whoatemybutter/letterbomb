How It Works
############

LetterBomb works by exploiting a buffer overflow exploit in the Wii Messaging Board.
By addressing a specific letter to the Wii (by using it's :doc:`mac_addresses`), you can cause the Wii to read such a
large message that it overflows and accidentally reads and executes the embedded Homebrew Channel inside of the
letter's file.

To know more about how it works, `watch the 25C3 video
<https://www.youtube.com/watch?v=DFvMsoBMIJk>`_ where the developers explain how they did this.