Tutorial
########

To fully install LetterBomb start to finish,
first ensure your Wii (and system) meets the requirements set out in :doc:`requirements`.

1. Get your Wii's MAC address and region.

	1. MAC: This is your Wii's "ID". See `this page`_.

	2. Region: This is where your Wii was manufactured. The first letter ID is what we want.

	+---------------+--------+-------+-------+
	| U             | E      | J     | K     |
	+===============+========+=======+=======+
	| United States | Europe | Japan | Korea |
	+---------------+--------+-------+-------+

2. Prepare SD/SDHC card.

	1. Make sure **it is not** an SDXC card; it must be SD or SDHC.
	2. If any data is on it, backup that data.
	3. `Format the SD card <https://www.wikihow.com/Format-an-SD-Card>`_ to either FAT16 or FAT32.

	.. warning::

		SDXC cards will not be recognized by the Wii. Only SD and SDHC cards are. Verify this before using the card.

3. :doc:`Install LetterBomb <installing>`.

4. Ensure that it's installed.

	.. code:: shell

		python -m letterbomb -v

	Verify the version matches the version you downloaded.

5. Use it.

	There are two ways to use letterbomb.

	You can either use :doc:`module`, which uses the CLI,
	or use :doc:`web`, which uses the web-service and has some dependencies.

	.. important::

		The web-service cannot be used without installing through the "pip+git" method.

.. _this page: https://en-americas-support.nintendo.com/app/answers/detail/a_id/2715/kw/mac/p/604/c/871/
