Installing
##########

1. Ensure your Wii and system meet the requirements in :doc:`requirements`.
2. Install ``letterbomb`` from PyPI or from Git:

	* Through PyPI

		.. important::

			This is for :doc:`CLI <cli>` and :doc:`module <module>` **only**.

		.. code:: shell

			python -m pip install -U letterbomb

	* Through Git:

		.. important::

			This is for :doc:`CLI <cli>`, :doc:`module <module>`, **and** the :doc:`web-service <web>`.

		.. code:: shell

			pip install git+https://gitlab.com/whoatemybutter/letterbomb

3. Run this to ensure it is working:

	.. code:: shell

		python -m letterbomb -v

	You should get ``LetterBomb v4.0`` back.